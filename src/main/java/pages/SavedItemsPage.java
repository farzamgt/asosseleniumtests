package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SavedItemsPage extends BasePage {

    @FindBy(css = "div[class*='itemCount']")
    private WebElement amountOfSavedItemsList;

    @FindBy(css = "h1.header_wnwkJ")
    private WebElement savedItemsHeader;

    @FindBy(css = "button[class*='deleteButton']")
    private List<WebElement> deleteButtonsList;

    @FindBy(css ="#saved-lists-app h2.noItemsTitle_uTxWD")
    private WebElement noSavedItemsMessage;

    private static final String NO_SAVED_ITEMS_MESSAGE = "You have no Saved Items";

    public SavedItemsPage(WebDriver driver) {
        super(driver);
    }

    public String getTextOfAmountOfSavedItems() {
        return amountOfSavedItemsList.getText();
    }

    public WebElement getAmountOfSavedItemsList() {
        return amountOfSavedItemsList;
    }

    public WebElement getNoSavedItemsMessage() {
        return noSavedItemsMessage;
    }

    public String getNoSavedItemsMessageText() {
        return noSavedItemsMessage.getText();
    }

    public void clickDeleteButtonOnAllItems() {
        for (WebElement delete : deleteButtonsList) {
            delete.click();
        }
    }

    public WebElement getSavedItemsHeader() {
        return savedItemsHeader;
    }
}
