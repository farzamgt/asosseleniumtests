package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    @FindBy(css = "#chrome-search")
    private WebElement searchField;

    @FindBy(css = "button[data-testid='search-button-inline']")
    private WebElement searchButton;

    @FindBy(css = "#women-floor")
    private WebElement womenCategoryButton;

    @FindBy(css = "button[data-id='415ffca1-8c1a-48a7-8997-9cc4929a7134']")
    private WebElement accessorizesCategoryButton;

    @FindBy(css = "a[href*='women/accessories/cat']")
    private WebElement allAccessorizesCategoryButton;

    @FindBy(css = "button[data-testid='myAccountIcon']")
    private WebElement accountIcon;

    @FindBy(css = "a[data-testid='signin-link']")
    private WebElement signInButton;

    @FindBy(css = "a[data-testid='signup-link']")
    private WebElement joinButton;

    @FindBy(css = "span.tiqiyps")
    private WebElement greetingMessage;

    @FindBy(css = "a[data-testid='myaccount-link']")
    private WebElement myAccountButton;

    private static final String HOME_PAGE_URL = "https://www.asos.com/";

    Actions action = new Actions(driver);

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage() {
        driver.get(HOME_PAGE_URL);
    }

    public void enterTextToSearchField(final String keyword) {
        searchField.clear();
        searchField.sendKeys(keyword);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public void clickWomenCategoryButton() {
        womenCategoryButton.click();
    }

    public void moveToAccessorizesCategoryButton() {
        action.moveToElement(accessorizesCategoryButton).build().perform();
    }

    public WebElement getAllAccessorizesCategoryButton() {
        return allAccessorizesCategoryButton;
    }

    public void clickBagsAndPursesCategoryButton() {
        allAccessorizesCategoryButton.click();
    }

    public void moveToAccountIcon() {
        action.moveToElement(accountIcon).build().perform();
    }

    public WebElement getAccountIcon() {
        return accountIcon;
    }

    public WebElement getSignInButton() {
        return signInButton;
    }

    public void clickSignInButton() {
        action.moveToElement(signInButton).build().perform();
        signInButton.click();
    }

    public WebElement getJoinButton() {
        return joinButton;
    }

    public void clickJoinButton() {
        joinButton.click();
    }

    public WebElement getMyAccountButton() {
        return myAccountButton;
    }

    public WebElement getGreetingMessage() {
        return greetingMessage;
    }

    public String getTextOfGreetingMessage() {
        return greetingMessage.getText();
    }
}
